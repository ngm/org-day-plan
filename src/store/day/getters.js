export function getEventsTodo (state) {
  return state.tasks.filter(task => task.type === 'TODO')
}

export function getEventsDone (state) {
  return state.tasks.filter(task => task.type === 'DONE')
}

export function getActiveTask (state) {
  const activeTask = state.tasks.find(task => task.active)
  return activeTask ?? { title: '', date: new Date(), end: new Date() }
}
