import { parse } from 'orga'
import { date } from 'quasar'

export function parseOrg ({ commit }, org) {
  commit('setOrg', org)

  const parsed = parse(org)
  const tasks = []
  let earliest = new Date()

  for (let i = 0; i < parsed.children.length; ++i) {
    const child = parsed.children[i]
    const keyword = child.children[0].keyword
    const content = child.children[0].content
    let start = null
    let end = null
    let active = false

    if (keyword === undefined) {
      continue
    }
    if (keyword === 'TODO') {
      start = child.children[1].timestamp.date
      end = child.children[1].timestamp.end
      if (start < earliest) {
        earliest = start
      }
      const currentTime = new Date()
      if (date.isBetweenDates(currentTime, start, end)) {
        active = true
      }
    } else if (keyword === 'DONE') {
      start = child.children[2].timestamp.date
      end = child.children[2].timestamp.end
    }
    const duration = (end.getTime() - start.getTime()) / 60000
    const time = start.toLocaleTimeString(
      [],
      { hour: '2-digit', minute: '2-digit' }
    )
    tasks.push({
      title: content,
      type: keyword,
      date: start,
      end: end,
      time: time,
      duration: duration,
      active: active
    })
  }

  commit('setEarliest', earliest)
  commit('setTasks', tasks)
}
