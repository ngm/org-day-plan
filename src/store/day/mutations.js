export function setOrg (state, org) {
  state.org = org
}

export function setEarliest (state, earliest) {
  state.earliest = earliest
}

export function setTasks (state, tasks) {
  state.tasks = tasks
}

export function setActiveTask (state, activeTask) {
  state.tasks.forEach(task => {
    if (task === activeTask) {
      task.active = true
    } else {
      task.active = false
    }
  })
}
